#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>

#define COMMAND_PIN D2

// Wireless network name and password (2.4GHz)
const char *ssid     = "";
const char *password = "";
// Security passphrase. Used to prvent unwanted interaction
String passphrase    = "";
// Set the hostname, useful in case you have internal DNS resolution
String hostname      = "";

WebServer server(80);

void processRelay() {
  if (!server.hasHeader("Authorization")) {
    server.send(403);
  }
  // We send a malformed Authorization header (no "Basic" keyword) on purpose.
  // Makes sense to go Lotek here. In exchange, you should make a really long passphrase
  String authReq = server.header("Authorization");

  if (authReq != passphrase) {
    server.send(403);
  }
  if (!server.hasArg("command")) {
    server.send(400, "application/json", "{\"message\": \"No command detected\"}");
  }

  if (server.arg("command") == "trigger") {
    digitalWrite(COMMAND_PIN, HIGH);
    server.send(200, "application/json", "{\"message\": \"Triggering Relay\"}");
    // A delay of 500ms should be enough for most applications, but feel free to change it.
    delay(500);
    digitalWrite(COMMAND_PIN, LOW);
  }
  else {
    server.send(400, "application/json", "{\"message\": \"Malformed request\"}");
  }
}

void stopSnooping(void) {
  // Default reponse to anything that is not correct
  server.send(200, "text/plain", "Hello there, were you looking for something? -_-");
}

void setup(void) {
  pinMode(COMMAND_PIN, OUTPUT);
  WiFi.setHostname(hostname.c_str());
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("[INFO] Connected to: ");
  Serial.println(ssid);
  Serial.print("[INFO] MAC Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("[INFO] IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp32")) {
    Serial.println("[ OK ] MDNS responder started");
  }

  server.on("/", stopSnooping);
  server.on("/relay", processRelay);
  server.onNotFound(stopSnooping);
  server.begin();
  Serial.println("[ OK ] HTTP server started");
}

void loop(void) {
  server.handleClient();
}
