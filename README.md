# AVOA Relay Control

Basic relay control for an `ESP32` Wifi/Bluetooth module.

This code will allow you to issue a command to the `ESP32` to trigger a relay (or anything else if you want).
It is part of an ongoing project called AVOA, mean to provide a simpler approach to automation for those
of us who find Home Assistant "maybe a bit too much for what we need".

Things that you can control with this:

- Anything that relies on am electrical pulse of some sort (real world examples: heating system when temp is lower than desired, garage door, fence door/property gate)

# Setting up

You will need to set up some values on the code before you upload it to the `ESP32`.

    const char *ssid     = "";  // Name of the WiFi network (2.4GHz)
    const char *password = "";  // Password of the WiFi network
    String passphrase    = "";  // Authentication passphrase, prevents unwanted use (recommended 32+ characters)
    String hostname      = "";  // Hostname for the ESP32, useful if you have DNS resolution in your LAN

# How can I test it?

If you have a computer at hand, you can use something like wget/curl (or in Windows, one of the [many different systems available](https://superuser.com/questions/299754/wget-curl-alternative-native-to-windows)). An example with curl would be:

    curl -i -X POST --header "Authorization: <YOUR AUTHORIZATION CODE>" http://<ESP32 IP ADDRESS>/relay\?command\=trigger

# Tested on

This code has been tested on the [DFRobot FireBeetle DFR0478 v4.0](https://wiki.dfrobot.com/FireBeetle_ESP32_IOT_Microcontroller(V3.0)__Supports_Wi-Fi_&_Bluetooth__SKU__DFR0478)

# License
MIT license
